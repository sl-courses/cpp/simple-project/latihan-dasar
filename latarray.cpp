#include <iostream>
using namespace std;

int main()
{
	awal:
	cout << "====================================================" << endl;
	cout << "PENGOLAHAN DATA ARRAY" << endl;
	cout << "====================================================" << endl;
	cout << "1. Isi data\n2. Cetak data\n3. Data terbesar\n4. Data terkecil" << endl;
	cout << "5. Hasil jumlah\n6. Rata-rata\n7. Selesai" << endl;
	cout << "====================================================" << endl;
	cout << "Masukkan pilihan anda : ";
	int pilihan;
	cin >> pilihan;
	int data[10];
	int i;
	int jumlah,terbesar,terkecil,sum,avg;
	switch (pilihan)
	{
	case 1:
		cout << "Masukkan data : ";
		for (i = 0; i < 10; i++)
		{
			cin >> data[i];
		}
		break;
	case 2:
		cout << "Data yang anda masukkan : ";
		for (i = 0; i < 10; i++)
		{
			cout << data[i] << " ";
		}
		cout << endl;
		break;
	case 3:
		terbesar = data[0];
		for (i = 0; i < 10; i++)
		{
			if (data[i] > terbesar)
			{
				terbesar = data[i];
			}
		}
		cout << "Data terbesar : " << terbesar << endl;
		break;
	case 4:
		terkecil = data[0];
		for (i = 0; i < 10; i++)
		{
			if (data[i] < terkecil)
			{
				terkecil = data[i];
			}
		}
		cout << "Data terkecil : " << terkecil << endl;
		break;
	case 5:
		sum = 0;
		for (i = 0; i < 10; i++)
		{
			sum += data[i];
		}
		cout << "Hasil jumlah : " << sum << endl;
		break;
	case 6:
		avg = sum / 10;
		cout << "Rata-rata : " << avg << endl;
		break;
	case 7:
		cout << "Terima kasih telah menggunakan program ini" << endl;
		exit(0);
	default:
		cout << "Pilihan anda tidak ada" << endl;
		break;
	}
	goto awal;
	return 0;
}
