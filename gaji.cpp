#include <iostream>
using namespace std;
int main ()
{
	string nama;
	int jabatan, tanggungan, masakerja, masakontrak, gol, gaji;
	double potongan;
	char a;
	cout << "Masukkan nama anda : ";
	cin >> nama;
	cout << "Masukkan jabatan anda. (1. CEO, 2. Manager, 3. Karyawan, 4. Staff) : ";
	cin >> jabatan;
	cout << "Masukkan tanggungan anda : ";
	cin >> tanggungan;
	cout << "Masukkan masa kerja anda. (1. 5 jam, 2. 8 jam, 3. 10 jam) : ";
	cin >> masakerja;
	cout << "==========================================================" << endl;
	cout << "Halo " << nama << ", Selamat datang di MyOffice" <<endl;
	if (jabatan == 1) //CEO
	{
		jabatan = 20000000;
		potongan = 0.4;
		gol = 10000000;
		masakontrak = 10;
	}
	else if (jabatan == 2) //Manager
	{
		jabatan = 10000000;
		potongan = 0.3;
		gol = 7000000;
		masakontrak = 5;
	}
	else if (jabatan == 3) //Karyawan
	{
		jabatan = 5000000;
		potongan = 0.2;
		gol = 4000000;
		masakontrak = 3;
	}
	else if (jabatan == 4) //Staff
	{
		jabatan = 2500000;
		potongan = 0.1;
		gol = 1000000;
		masakontrak = 2;
	}
	else
	{
		cout << "Jabatan tidak ditemukan" << endl;
	}
	if (masakerja == 1)
	{
		masakerja = 500000;
	}
	else if (masakerja == 2)
	{
		masakerja = 1000000;
	}
	else if (masakerja == 3)
	{
		masakerja = 2000000;
	}
	else
	{
		cout << "Masa kerja tidak ditemukan" << endl;
	}
	if (tanggungan == 1)
	{
		tanggungan = 1000000;
	}
	else if (tanggungan == 2)
	{
		tanggungan = 2000000;
	}
	else if (tanggungan == 3)
	{
		tanggungan = 3000000;
	}
	else if (tanggungan >= 4)
	{
		tanggungan = 5000000;
	}
	else if (tanggungan == 0)
	{
		tanggungan = 100000;
	}
	gaji = gol + masakerja + jabatan + tanggungan;
	potongan = gaji * potongan;
	gaji = gaji - potongan;
	cout << "Ada yang bisa saya bantu? (y/n)" << endl;
	cin >> a;
	if (a=='y')
	{
		menu:
		cout << "Silahkan memilih menu yang tersedia :"  << endl;
		cout << "1. Cek gaji" << endl;
		cout << "2. Cek masa kontrak kerja" << endl;
		cout << "3. Keluar" << endl;
		cin >> a;
		if (a=='1')
		{
			cout << "Gaji anda adalah Rp. " << gaji << endl;
			cout << "Ada lagi yang bisa saya bantu? (y/n)" << endl;
			cin >> a;
			if (a=='y')
			{
				goto menu;
			}
			else
			{
				cout << "Terima kasih telah menggunakan layanan kami" << endl;
			}
		}
		else if (a=='2')
		{
			cout << "Masa kontrak kerja anda adalah " << masakontrak << " tahun" << endl;
			cout << "Ada lagi yang bisa saya bantu? (y/n)" << endl;
			cin >> a;
			if (a=='y')
			{
				goto menu;
			}
			else
			{
				cout << "Terima kasih telah menggunakan layanan kami" << endl;
			}
		}
		else if (a=='3')
		{
			cout << "Terima kasih telah menggunakan MyOffice" << endl;
		}
		else
		{
			cout << "Menu tidak ditemukan" << endl;
		}
	}
	else if (a=='n')
	{
		cout << "Terima kasih telah menggunakan MyOffice" << endl;
	}
	else
	{
		cout << "Pilihan tidak tersedia" << endl;
	}
	return 0;					
}
