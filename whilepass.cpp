#include <iostream>
using namespace std;

string password;
string login = "admin";
int a,b;

void whilepass()
{
	//membuatloginpassword
	cout << "Masukkan kata sandi : ";
	cin >> password;
	while (password != login)
	{
		a++;
		b = 4;
		cout << "Login gagal.\nKesempatan tersisa : " << b - a << endl;
		cout << "Masukkan kata sandi : ";
		cin >> password;
		if (a == 3)
		{
			cout << "Kesempatan habis." << endl;
			cout << "Akun anda sementara dibekukan." << endl;
			break;
		}
	}
}
void forpass()
{
	//membuatloginpassword
	cout << "Masukkan kata sandi : ";
	cin >> password;
	if (password == login)
	{
		cout << "Login berhasil" << endl;
	}
	else
	{
		cout << "Login gagal.\nMasukkan sandi yang benar." << endl;
		for (int i = 1; i <= 3; i++)
		{
			cout << "Masukkan kata sandi : ";
			cin >> password;
			if (password == login)
			{
				cout << "Login berhasil" << endl;
				break;
			}
			else
			{
				cout << "Login gagal" << endl;
				cout << "Kesempatan tersisa : " << 3 - i << endl;
			}
			if (i == 3)
			{
				cout << "Akun anda sementara dibekukan." << endl;
			}
		}
	}
}
void dopass()
{
	//membuatloginpassword
	do 
	{
		b = 4;
		a++;
		cout << "Masukkan kata sandi : ";
		cin >> password;
		cout << "Kesempatan tersisa : " << b-a << endl;
		if (a == 4)
		{
			cout << "Akun anda sementara dibekukan." << endl;
			break;
		}
	}
	while (password != login);
	cout << "Login berhasil" << endl;
}
int main ()
{
	cout << "Pilih metode login : " << endl;
	cout << "1. While" << endl;
	cout << "2. For" << endl;
	cout << "3. Do-While" << endl;
	int pilih;
	cin >> pilih;
	switch (pilih)
	{
		case 1:
			whilepass();
			break;
		case 2:
			forpass();
			break;
		case 3:
			dopass();
			break;
		default:
			cout << "Pilihan tidak ada" << endl;
			break;
	}
	return 0;
}
