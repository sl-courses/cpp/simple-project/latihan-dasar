#include <iostream>
#include <algorithm>
using namespace std;
int main()
{
	cout << "Ranking 3 besar" << endl;
	int nilai[3] = {};
	string nama[3] = {};
	for (int i = 0; i < 3; i++)
	{
		cout << "Masukkan nama siswa : ";
		getline(cin>>ws, nama[i]);
		cout << "Masukkan nilai siswa : ";
		cin >> nilai[i];
	}
	for (int i = 0; i < 3; i++)
	{
		for (int j = i + 1; j < 3; j++)
		{
			if (nilai[i] < nilai[j])
			{
				swap(nilai[i], nilai[j]);
				swap(nama[i], nama[j]);
			}
		}
	}
	cout << "Nama\t\tNilai\n";
	for (int i = 0; i < 3; i++)
	{
		cout << nama[i] << "\t" << nilai[i] << endl;
	}
	return 0;
}
