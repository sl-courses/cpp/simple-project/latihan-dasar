#include <iostream>
 
using namespace std;
 
int main()
{
	cout <<"==========================================================" << endl;
	cout << "Program segitiga Angka" << endl;
	int tinggi,i,j;
	cout << "Input tinggi segitiga: ";
	cin >> tinggi;
	cout << "==================================" << endl;
	for(i=1;i<=tinggi;i++) 
	{
		cout << i << " ";
		for(j=1;j<i;j++) 
		{
			cout << (j+i)+i <<" ";
		}
		cout << endl;
	}
 
  return 0;
}
