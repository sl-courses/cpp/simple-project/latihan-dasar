// Simulate bank deposits and withdrawals
#include <iostream>
using namespace std;
int main()
{
   // Local variable declarations 
	int Command = 0;
	int Money = 0;
	int Balance = 100;
   // Print command prompt
	cout << "Enter command number: " <<endl;
   // Read command
	cout << "0 - quit" <<endl;
	cout << "1 - deposit" <<endl;
	cout << "2 - withdraw\n";
	cout << "3 - current balance\n";

	// Read and handle banking commands
	cin >> Command;
	switch (Command)
	{
		case 0: // Quit code
			cout << "See you later!" << endl;
			break;
		case 1: // Deposit code
			cout << "Enter deposit amount: ";
			cin >> Money;
			if (Money < 0)
			{
				cout << "Invalid deposit amount" << endl;
				return 0;
			}
			Balance = Balance + Money;
			break;
		case 2: // Withdraw code
			cout << "Enter withdraw amount: ";
			cin >> Money;
			Balance = Balance - Money;
			if (Money > Balance)
			{
				cout << "Saldo anda terlalu besar" << endl;
				return 0;
			}
			break;
		case 3: // Print balance code
			cout << "Current balance = " << Balance << endl;
			break;
		default: // Handle other values
			cout << "Ooops try again" << endl;
			break;
   	}
// Print final balance
	cout << "Final balance = " << Balance << endl;
	return 0;
}
