#include <iostream>

using namespace std;
struct segitiga{
	float alas, tinggi;
};
void segi(float &b, float &c)
{
    b = 0.5 * b * c;
}
int main()
{
	int n;
    cout << "Masukkan jumlah segitiga : ";
    cin >> n;
	segitiga a[n];
    for (int i = 0; i < n; i++)
	{
		cout << "Masukkan alas ["<<i+1<<"] : ";
		cin >> a[i].alas;
		cout << "Masukkan tinggi ["<<i+1<<"] : ";
		cin >> a[i].tinggi;
	}
	for (int i = 0; i < n; i++)
	{
		segi(a[i].alas, a[i].tinggi);
		cout << "Luas segitiga ["<<i+1<<"] : " << a[i].alas << endl;
	}
	

}
