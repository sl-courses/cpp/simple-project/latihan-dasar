#include <iostream>
using namespace std;

int main()
{
    string nama, k, cari;
    int i, j;
    int posisi;
    cout << "Masukkan nama anda : ";
    cin >> nama;
    cout << "Nama anda : " << nama << endl;
    cout << "Program mengganti nama" << endl;
    cout << "Masukkan kata yang akan diganti : ";
    cin >> cari;
    posisi = nama.find(cari);
    cout << "Teks nya : ";
    cin >> k;
    nama = nama.replace(posisi,nama.length(),k);
    cout << "Nama anda : " << nama << endl;
	cout << "Program menambah nama" << endl;
	cout << "Kata akan ditambahkan sebelum huruf : ";
	cin >> cari;
	posisi = nama.find(cari);
	cout << "Masukkan kata yang akan ditambahkan : ";
	cin >> k;
	nama = nama.insert(posisi,k);
	cout << "Nama anda : " << nama << endl;
	cout << "Program menghapus nama" << endl;
	cout << "Masukkan kata yang akan dihapus : ";
	cin >> cari;
	posisi = nama.find(cari);
	nama = nama.erase(posisi,nama.length());
	cout << "Nama anda : " << nama << endl;

    return 0;
}
